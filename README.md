# An exploration harness for Redis

The purpose of this Kotlin project is to provide a quick and simple way to explore and experiment with basic Redis functionality.

## Setup
* Install - follow instructions on https://redis.io/topics/quickstart. Building from sources on Ubuntu 19.04 is quick 
and painless.  
* Run a local instance of Redis with the default port:
```
$ ./redis-server 
17764:C 07 May 2020 16:25:22.900 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
17764:C 07 May 2020 16:25:22.900 # Redis version=6.0.1, bits=64, commit=00000000, modified=0, pid=17764, just started
17764:C 07 May 2020 16:25:22.900 # Warning: no config file specified, using the default config. In order to specify a config file use ./redis-server /path/to/redis.conf
                _._                                                  
           _.-``__ ''-._                                             
      _.-``    `.  `_.  ''-._           Redis 6.0.1 (00000000/0) 64 bit
  .-`` .-```.  ```\/    _.,_ ''-._                                   
 (    '      ,       .-`  | `,    )     Running in standalone mode
 |`-._`-...-` __...-.``-._|'` _.-'|     Port: 6379
 |    `-._   `._    /     _.-'    |     PID: 17764
  `-._    `-._  `-./  _.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |           http://redis.io        
  `-._    `-._`-.__.-'_.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |                                  
  `-._    `-._`-.__.-'_.-'    _.-'                                   
      `-._    `-.__.-'    _.-'                                       
          `-._        _.-'                                           
              `-.__.-'                                               
```
* Read and run the tests

---
## Reference

* Go through data types intro https://redis.io/topics/data-types-intro
* Implement tests for data types mentioned above
* Scripting - peruse https://redis.io/commands/eval and write some tests
* Transactions - https://redis.io/topics/transactions and write some tests
* GUI - there is an official GUI interface (redis insight) that can be used via Docker ( https://docs.redislabs.com/latest/ri/installing/install-docker/ )
* Streams - https://redis.io/topics/streams-intro and write some tests
* Streams - "Redis Streams Featuring Salvatore Sanfilippo" https://www.youtube.com/watch?v=qXEyuUxQXZM
* Streams - the `streams` package implements some of the demo code in the above video.


