package streams

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset.offset
import org.junit.Before
import org.junit.Test
import redis.clients.jedis.Jedis
import redis.clients.jedis.StreamEntry
import reset
import verify

private val redis = Jedis("localhost")

class Producer1Test {

    private val producer = Producer1(streamName = "stream")

    @Before
    fun before() {
        reset(redis)
        redis.xtrim("stream", 0, false)
    }

    @Test
    fun `test XLEN XRANGE`() {
        Thread(producer).start()
        Thread.sleep(3 * 1_000)
        producer.stop = true

        redis.xlen("stream").verify {
            assertThat(it).isGreaterThan(2)
            assertThat(it).isCloseTo(3, offset(2L))
        }

        // query entire stream
        val resultAll: List<StreamEntry> = redis.xrange("stream", null, null, 100).verify {
//            println(it)
            assertThat(it.size).isGreaterThan(2)
            assertThat(it.size).isCloseTo(3, offset(3))
        }

        // query excluding the first element
        redis.xrange("stream", resultAll[1].id, null, 100).verify {
//            println(it)
            assertThat(it.size).isEqualTo(resultAll.size - 1)
            assertThat(it[0].id).isEqualTo(resultAll[1].id)
            assertThat(it[1].id).isEqualTo(resultAll[2].id)
        }

    }
}