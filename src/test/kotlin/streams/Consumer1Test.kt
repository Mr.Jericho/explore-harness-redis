package streams

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset.offset
import org.junit.Before
import org.junit.Test
import redis.clients.jedis.Jedis
import reset

private val redis = Jedis("localhost")

class Consumer1Test {

    @Before
    fun before() {
        reset(redis)
        redis.xtrim("stream", 0, false)
    }

    @Test
    fun `blocking XREAD consumes from last entry`() {
        val producer = Producer1().also { Thread(it).start() }
        Thread.sleep(3 * 1_000)

        var ctr = 0
        val missedCount = redis.xlen("stream")

        val consumer  = Consumer1(callBack = { println(it); ctr++}).also { Thread(it).start() }
        Thread.sleep(2 * 1_000)

        producer.stop = true
        consumer.stop = true

        val totalCount =  redis.xlen("stream")
        assertThat(missedCount + ctr).isCloseTo(totalCount, offset(1L))
    }
}