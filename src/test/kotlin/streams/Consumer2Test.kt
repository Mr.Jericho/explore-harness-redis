package streams

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset
import org.assertj.core.data.Offset.offset
import org.junit.Before
import org.junit.Test
import redis.clients.jedis.Jedis
import redis.clients.jedis.StreamEntryID
import redis.clients.jedis.StreamEntryID.LAST_ENTRY
import reset

private val redis = Jedis("localhost")

class Consumer2Test {

    @Before
    fun before() {
        reset(redis)
        redis.xtrim("stream", 0, false)
    }

    @Test
    fun `blocking XREAD consumes from last processed entry`() {

        val consumedIDs = mutableListOf<StreamEntryID>()

        fun processResult(result: XREADResult) {
            println(result);
            result!![0].value.forEach { consumedIDs.add(it.id) }
        }

        val producer = Producer1().also { Thread(it).start() }
        Thread.sleep(3 * 1_000)
        val missedCount = redis.xlen("stream")

        var consumer = Consumer2(lastId = LAST_ENTRY, callBack = { processResult(it) }).also { Thread(it).start() }
        Thread.sleep(3 * 1_000)

        consumer.stop = true

        Thread.sleep(3 * 1_000)
        consumer = Consumer2(lastId = consumedIDs.last(), callBack = { processResult(it) }).also { Thread(it).start() }
        Thread.sleep(3 * 1_000)

        producer.stop = true
        consumer.stop = true

        val totalCount = redis.xlen("stream")
        assertThat(missedCount + consumedIDs.size).isCloseTo(totalCount, offset(3L))
    }
}