import redis.clients.jedis.Jedis
import redis.clients.jedis.StreamEntryID
import java.util.AbstractMap

fun reset(jedis : Jedis) {
    jedis.flushAll()
    jedis.scriptFlush()
}

fun <T> T.verify(block: (T) -> Unit): T = this.also(block)

fun nextStreamEntryID(last : StreamEntryID) : StreamEntryID =
    StreamEntryID(last.time, last.sequence+1)


