package org.example

import nextStreamEntryID
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisPool
import redis.clients.jedis.JedisPoolConfig
import redis.clients.jedis.StreamEntry
import redis.clients.jedis.StreamEntryID.LAST_ENTRY
import reset
import streams.queryStream
import verify


// https://redis.io/topics/streams-intro

private val redis = Jedis("localhost")
private const val stream1 = "stream1"
private const val stream2 = "stream2"

class StreamTest {

    private val testData2 = listOf(
        mapOf("sensor-id" to "1", "temperature" to "19.8"),
        mapOf("sensor-id" to "2", "temperature" to "10.0")
    )
    private val testData4 = listOf(
        mapOf("sensor-id" to "3", "temperature" to "19.8"),
        mapOf("sensor-id" to "4", "temperature" to "10.0"),
        mapOf("sensor-id" to "1", "temperature" to "21.0"),
        mapOf("sensor-id" to "2", "temperature" to "9.1")
    )


    @Before
    fun before() {
        reset(redis)
        redis.xtrim(stream1, 0, false)
        redis.xtrim(stream2, 0, false)
    }

    @Test
    fun `XADD XLEN XTRIM`() {
        with(redis) {
            testData2.forEach { xadd(stream1, null, it) }

            xlen(stream1).verify { assertThat(it).isEqualTo(testData2.size.toLong()) }
            xtrim(stream1, 0, false).verify { assertThat(it).isEqualTo(testData2.size.toLong()) }
            xlen(stream1).verify { assertThat(it).isEqualTo(0) }
        }
    }

    @Test
    fun `XRANGE - +`() {
        with(redis) {
            testData2.forEach { xadd(stream1, null, it) }

            val result: List<StreamEntry> = xrange(stream1, null, null, 100)
            //e.g: [1588841057149-0 {temperature=19.8, sensor-id=123}, 1588841057149-1 {temperature=10.0, sensor-id=456}]
            result.verify {
                assertThat(it.size).isEqualTo(testData2.size)
                it.map { entry -> entry.fields }
                    .forEachIndexed { i, field -> assertThat(field).isEqualTo(testData2[i]) }
            }

        }
    }

    @Test
    fun `XRANGE incremental`() {
        with(redis) {
            testData4.forEach { xadd(stream1, null, it) }

            val result: List<StreamEntry> = xrange(stream1, null, null, 2)

            val lastID = result.last().id
            val nextID = nextStreamEntryID(lastID)

            val nextResult: List<StreamEntry> = xrange(stream1, nextID, null, 2)

            nextResult.verify {
                assertThat(it.size).isEqualTo(2)
                it.map { entry -> entry.fields }
                    .forEachIndexed { i, field -> assertThat(field).isEqualTo(testData4[2 + i]) }
            }
        }
    }

    @Test
    fun `XREVRANGE`() {
        with(redis) {
            testData4.forEach { xadd(stream1, null, it) }

            // use to find last stream item
            val result: List<StreamEntry> = xrevrange(stream1, null, null, 1)
            val nextID = nextStreamEntryID(result[0].id)

            xadd(stream1, null, mapOf("sensor-id" to "10", "temperature" to "10"))

            val nextResult: List<StreamEntry> = xrange(stream1, nextID, null, 2)

            nextResult.verify {
                assertThat(it.size).isEqualTo(1)
                assertThat(it.first().fields).isEqualTo(mapOf("sensor-id" to "10", "temperature" to "10"))
            }
        }
    }

    @Test
    fun `XREAD`() {
        with(redis) {
            testData4.forEach { xadd(stream1, null, it) }
            testData2.forEach { xadd(stream2, null, it) }

            // read all from stream1
            xread(0, 0, queryStream(stream1)).verify {
                assertThat(it).hasSize(1)
                assertThat(it.first().value).hasSameSizeAs(testData4)
            }
            //read first 2
            xread(2, 0, queryStream(stream1)).verify {
                assertThat(it).hasSize(1)
                assertThat(it.first().value).hasSize(2)
            }

            // read from 2 streams
            xread(0, 0, queryStream(stream1), queryStream(stream2)).verify {
                assertThat(it).hasSize(2)
                it.map { it }.filter { it.key == stream1 }[0].verify {
                    assertThat(it.value).hasSameSizeAs(testData4)
                }
                it.map { it }.filter { it.key == stream2 }[0].verify {
                    assertThat(it.value).hasSameSizeAs(testData2)
                }
            }
        }
    }

    @Test
    fun `XREAD blocking`() {
        val pool = JedisPool(JedisPoolConfig(), "localhost")

        val readThread = Thread {
            pool.resource.use {
                it.xread(0, 3 * 1000, queryStream(stream1, LAST_ENTRY)).verify {
                    assertThat(it).hasSize(1)
                    assertThat(it.first().value[0].fields).isEqualTo(testData2[0])
                }
                it.xread(0, 3 * 1000, queryStream(stream1, LAST_ENTRY)).also {
                    assertThat(it).hasSize(1)
                    assertThat(it.first().value[0].fields).isEqualTo(testData4[0])
                }
            }
        }.also { it.start() }

        Thread {
            pool.resource.use {
                Thread.sleep(1 * 1000)
                testData2.forEach { item -> it.xadd(stream1, null, item) }

                Thread.sleep(1 * 1000)
                testData4.forEach { item -> it.xadd(stream1, null, item) }
            }
        }.also { it.start() }

        readThread.join()
    }


}