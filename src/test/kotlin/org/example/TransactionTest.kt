package org.example
// https://redis.io/topics/transactions
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import redis.clients.jedis.Jedis
import reset
import verify

private val redis = Jedis("localhost")

class TransactionTest {

    @Before
    fun before() {
        reset(redis)
    }

    @Test
    fun `MULTI EXEC`() {
        with(redis) {
            val transaction = multi()

            val result = with(transaction) {
                set("foo", "bar")
                incrBy("ctr", 10)

                exec()
            }

            result.verify {
                println(it)
                assertThat(it).hasSize(2)
                assertThat(it[0]).isEqualTo("OK")
                assertThat(it[1]).isEqualTo(10.toLong())
            }

            redis.get("foo").verify { assertThat(it).isEqualTo("bar") }
            redis.get("ctr").verify { assertThat(it).isEqualTo("10") }
        }
    }

    @Test
    fun `MULTI DISCARD`() {
        with(redis) {
            val transaction = multi()

            with(transaction) {
                set("foo", "bar")
                incrBy("ctr", 10)

                discard()
            }

            redis.get("foo").verify { assertThat(it).isNull() }
            redis.get("ctr").verify { assertThat(it).isNull() }
        }
    }

}