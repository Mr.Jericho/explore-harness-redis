package org.example

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import redis.clients.jedis.Jedis
import reset
import verify

private val redis = Jedis("localhost")

class ScriptTest {

    @Before
    fun before() {
        reset(redis)
    }

    @Test
    fun `EVAL`() {
        val script = "return {KEYS[1],KEYS[2],ARGV[1],ARGV[2]}"
        with(redis) {
            eval(script, 2, "key1", "key2", "A", "B").verify {
                assertThat(it).isEqualTo(listOf("key1", "key2", "A", "B"))
            }
        }
    }

    @Test
    fun `EVAL redis call`() {
        val script = "return redis.call('set',KEYS[1], KEYS[2])"
        with(redis) {
            eval(script, 2, "foo", "bar").verify { assertThat(it).isEqualTo("OK") }
            get("foo").verify { assertThat(it).isEqualTo("bar") }
        }
    }

    @Test
    fun `unique value list using a backung set`() {
        val script = """
            if redis.call('SADD', KEYS[1], ARGV[1]) == 1 then
                redis.call('LPUSH', KEYS[2], ARGV[1] )
            end
        """.trimIndent()

        with(redis) {
            eval(script, 2, "set", "list", "1")
            eval(script, 2, "set", "list", "2")
            eval(script, 2, "set", "list", "1")
            eval(script, 2, "set", "list", "2")
            eval(script, 2, "set", "list", "3")

            lrange("list", 0, -1).verify { assertThat(it).isEqualTo(listOf("3", "2", "1")) }
        }

    }
}