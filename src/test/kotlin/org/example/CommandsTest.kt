package org.example

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import redis.clients.jedis.Jedis
import redis.clients.jedis.Tuple
import redis.clients.jedis.params.SetParams
import redis.clients.jedis.params.ZAddParams
import reset
import verify
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertNull


private val redis = Jedis("localhost")

class RedisTest {

    @Before
    fun before() {
        reset(redis)
    }

    @Test
    fun `SET GET`() {
        with(redis) {
            set("a:b:c", "D").also { assertEquals("OK", it) }
            get("a:b:c").also { assertEquals("D", it) }
            // set replaces...
            set("a:b:c", "Z").also { assertEquals("OK", it) }
            get("a:b:c").also { assertEquals("Z", it) }
        }
    }

    @Test
    fun `SET GET NX`() {
        with(redis) {
            set("a:b:c", "D").also { assertEquals("OK", it) }
            set("a:b:c", "D", SetParams.setParams().nx()).also { assertNull(it) }
        }
    }

    @Test
    fun `SET GET XX`() {
        with(redis) {
            set("a:b:c", "D", SetParams.setParams().xx()).also { assertNull(it) }
        }
    }

    @Test
    fun `INCR`() {
        with(redis) {
            set("counter", "1")
            incr("counter")
            incrBy("counter", 2)

            get("counter").also { assertThat(it).isEqualTo("4") }
        }
    }

    @Test
    fun `MSET MGET`() {
        with(redis) {
            mset("a", "1", "b", "2")
            mget("a", "b", "z").also { assertThat(it).isEqualTo(listOf("1", "2", null)) }
        }
    }

    @Test
    fun `EXISTS DEL`() {
        with(redis) {
            set("a", "1")

            assertThat(exists("a")).isEqualTo(true)
            assertThat(exists("z")).isEqualTo(false)

            del("a").also { assertThat(it).isEqualTo(1) }
            del("z").also { assertThat(it).isEqualTo(0) }
            assertThat(exists("a")).isEqualTo(false)
        }
    }


    @Test
    fun `lists LPUSH RPUSH LRANGE`() {
        with(redis) {
            lpush("list", "a", "b", "c").also { assertThat(it).isEqualTo(3) }
            lrange("list", 0, -1).also { assertThat(it).isEqualTo(listOf("c", "b", "a")) }

            rpush("list", "1", "2", "3")
            lrange("list", 0, -1).also { assertThat(it).isEqualTo(listOf("c", "b", "a", "1", "2", "3")) }
        }
    }

    @Test
    fun `lists LPOP LPUSH RPOP`() {
        with(redis) {
            lpush("list", "a", "b", "c")

            lpop("list").also { assertThat(it).isEqualTo("c") }
            lpop("list").also { assertThat(it).isEqualTo("b") }
            lpop("list").also { assertThat(it).isEqualTo("a") }
            lpop("list").also { assertThat(it).isNull() }

            lpush("list", "a", "b", "c")

            rpop("list").also { assertThat(it).isEqualTo("a") }
            rpop("list").also { assertThat(it).isEqualTo("b") }
            rpop("list").also { assertThat(it).isEqualTo("c") }
            rpop("list").also { assertThat(it).isNull() }
        }
    }

    @Test
    fun `lists LTRIM`() {
        with(redis) {
            rpush("list", "1", "2", "3", "4", "5")
            ltrim("list", 0, 2)

            lrange("list", 0, -1).also { assertThat(it).isEqualTo(listOf("1", "2", "3")) }
        }
    }

    @Test
    fun `lists BRPOP`() {

        Thread {
            Jedis("localhost").use {
                Thread.sleep(2_000)
                it.lpush("list1", "1")
            }
        }.start()

        with(redis) {
            brpop(3, "list1", "list2").verify { assertThat(it).isEqualTo(listOf("list1", "1")) }
        }
    }

    @Test
    fun `lists RPOPLPUSH`() {
        with(redis) {
            rpush("list1", "1", "2", "3")

            rpoplpush("list1", "list2")
            rpoplpush("list1", "list2")

            lrange("list1", 0, -1).verify { assertThat(it).isEqualTo(listOf("1")) }
            lrange("list2", 0, -1).verify { assertThat(it).isEqualTo(listOf("2", "3")) }
        }
    }

    @Test
    fun `hashes HMSET HMGET HGET HMGETALL HINCRBY`() {
        with(redis) {
            hmset("hash", mapOf("userId" to "abc", "year" to "2020"))

            hmget("hash", "userId", "year", "foo").verify { assertThat(it).isEqualTo(listOf("abc", "2020", null)) }
            hget("hash", "userId").verify { assertThat(it).isEqualTo("abc") }
            hgetAll("hash").verify { assertThat(it).isEqualTo(mapOf("userId" to "abc", "year" to "2020")) }

            hincrBy("hash", "year", -10).verify { assertThat(it).isEqualTo(2010) }
            hgetAll("hash").verify { assertThat(it).isEqualTo(mapOf("userId" to "abc", "year" to "2010")) }
        }
    }

    @Test
    fun `sets SADD SMEMBERS SISMEMBER`() {
        with(redis) {

            sadd("set", "1", "2", "3", "1").verify { assertThat(it).isEqualTo(3) }
            smembers("set").verify { assertThat(it).isEqualTo(setOf("1", "2", "3")) }
            sismember("set", "1").verify { assertThat(it).isEqualTo(true) }
            sismember("set", "foo").verify { assertThat(it).isEqualTo(false) }


        }
    }

    @Test
    fun `sets tags SINTER`() {
        val tags = mapOf("1" to "one", "2" to "two", "77" to "seventyseven")
        with(redis) {
            sadd("news:100:tags", "1", "77")
            sadd("news:101:tags", "2", "1")

            sadd("tags:1:news", "100", "101")
            sadd("tags:2:news", "100")
            sadd("tags:77:news", "101")

            smembers("news:100:tags")
                .map { tags[it] }
                .verify { assertThat(it).isEqualTo(listOf("one", "seventyseven")) }

            sinter("news:100:tags", "news:101:tags").verify { assertThat(it).isEqualTo(setOf("1")) }
        }
    }


    @Test
    fun `sets SPOP SUNIONSTORE SCARD`() {
        with(redis) {
            sadd("set", "1", "2", "3")

            sunionstore("newset", "set")

            spop("newset").verify { assertThat(it).isIn("1", "2", "3") }
            spop("newset").verify { assertThat(it).isIn("1", "2", "3") }
            spop("newset").verify { assertThat(it).isIn("1", "2", "3") }
            spop("newset").verify { assertThat(it).isNull() }

            smembers("set").verify { assertThat(it).isEqualTo(setOf("1", "2", "3")) }
            scard("set").verify { assertThat(it).isEqualTo(3) }
        }
    }

    @Test
    fun `ordered sets ZADD ZRANGE ZRANGEWITHSCORES ZRANGEBYSCORE ZREMRANGEBYSCORE`() {
        with(redis) {
            zadd(
                "oset", mapOf(
                    "Alan Kay" to 1940.0,
                    "Sophie Wilson" to 1957.0,
                    "Richard Stallman" to 1953.0,
                    "Anita Borg" to 1949.0,
                    "Yukihiro Matsumoto" to 1965.0,
                    "Hedy Lamarr" to 1914.0,
                    "Claude Shannon" to 1916.0,
                    "Linus Torvalds" to 1969.0,
                    "Alan Turing" to 1912.0
                )
            )
            zrange("oset", 0, -1).verify {
                assertThat(it).isEqualTo(
                    setOf(
                        "Alan Turing", "Hedy Lamarr", "Claude Shannon", "Alan Kay", "Anita Borg", "Richard Stallman",
                        "Sophie Wilson", "Yukihiro Matsumoto", "Linus Torvalds"
                    )
                )
            }

            zrangeWithScores("oset", 0, 1).verify {
                assertThat(it).isEqualTo(
                    setOf(
                        Tuple("Alan Turing", 1912.0),
                        Tuple("Hedy Lamarr", 1914.0)
                    )
                )
            }

            zrangeByScore("oset", 0.0, 1920.0).verify {
                assertThat(it).isEqualTo(setOf("Alan Turing", "Hedy Lamarr", "Claude Shannon"))
            }

            zremrangeByScore("oset", 1940.0, 1960.0).verify { assertThat(it).isEqualTo(4) }
            zrange("oset", 0, -1).verify {
                assertThat(it).isEqualTo(
                    setOf("Alan Turing", "Hedy Lamarr", "Claude Shannon", "Yukihiro Matsumoto", "Linus Torvalds")
                )
            }
        }
    }

    @Test
    fun `ordered sets ZRANGEBYLEX`() {
        with(redis) {
            zadd("oset", mapOf("z" to 0.0, "y" to 0.0, "x" to 0.0, "a" to 0.0, "b" to 0.0))
            zrange("oset", 0, -1).verify { assertThat(it).isEqualTo(setOf("a", "b", "x", "y", "z")) }
            zrangeByLex("oset", "(a", "[y").verify { assertThat(it).isEqualTo(setOf("b", "x", "y")) }
        }
    }


    @Test
    fun `ordered sets ZPOPMIN unique value FIFO queue`() {

        fun millis(): Double = Instant.now().toEpochMilli().toDouble()

        with(redis) {
            zadd("oset", millis(), "c", ZAddParams.zAddParams().nx());
            zadd("oset", millis(), "b", ZAddParams.zAddParams().nx());
            zadd("oset", millis(), "a", ZAddParams.zAddParams().nx());
            zadd("oset", millis(), "d", ZAddParams.zAddParams().nx());
            zadd("oset", millis(), "a", ZAddParams.zAddParams().nx());

            println(zpopmin("oset"))
            println(zpopmin("oset"))
            println(zpopmin("oset"))
            println(zpopmin("oset"))
            println(zpopmin("oset"))
        }
    }


    @Test
    fun `bitmaps  SETBIT GETBIT BITCOUNT`() {
        with(redis) {
            setbit("key", 100, true).verify {
                assertThat(getbit("key", 100)).isEqualTo(true)
                assertThat(getbit("key", 1)).isEqualTo(false)
            }
            setbit("key", 1, true).verify {
                assertThat(bitcount("key")).isEqualTo(2)
                assertThat(bitpos("key", true)).isEqualTo(1)
            }
        }
    }


}
