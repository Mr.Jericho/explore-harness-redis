package streams

import redis.clients.jedis.Jedis
import redis.clients.jedis.StreamEntry
import redis.clients.jedis.StreamEntryID
import redis.clients.jedis.StreamEntryID.LAST_ENTRY
import kotlin.collections.Map.Entry


/**
 * Following the presentation at https://www.youtube.com/watch?v=qXEyuUxQXZM
 * Demonstrates blocking XREAD with "persistent" lastId - see Consumer2Test
 */
class Consumer2(private val streamName: String = "stream", var lastId : StreamEntryID, val callBack : (XREADResult) -> Unit) : Runnable {

    @Volatile
    var stop = false

    private val redis = Jedis("localhost")

    override fun run() {
        while (!stop) {
            redis.xread(0, 2 * 1_000, queryStream(streamName, lastId))?.let {
                lastId = it[0].value[0].id
                callBack(it)
            }
        }
    }
}
