package streams

import redis.clients.jedis.StreamEntry
import redis.clients.jedis.StreamEntryID
import java.util.*
import kotlin.random.Random.Default.nextDouble

typealias XREADResult =  List<Map.Entry<String, List<StreamEntry>>>?

var counter: Long = 0
fun genPayload(id: String) = mapOf("id" to id, "counter" to "${counter++}", "value" to "${20 + (nextDouble() - nextDouble())}")

// by  default query from 0-0 == StreamEntryID() == null
fun queryStream(streamName: String, id : StreamEntryID? = null) : AbstractMap.SimpleImmutableEntry<String, StreamEntryID?> =
    AbstractMap.SimpleImmutableEntry(streamName, id)
