package streams

import redis.clients.jedis.Jedis

/**
 * Following the presentation at https://www.youtube.com/watch?v=qXEyuUxQXZM
 * Demonstrates range queries - see Producer1Test
 */
class Producer1(private val streamName: String = "stream") : Runnable {

    @Volatile
    var stop = false

    private val redis = Jedis("localhost")
    private val stationId = "${ProcessHandle.current().pid()}"

    override fun run() {
        while (!stop) {
            redis.xadd(streamName, null, genPayload(stationId))
            Thread.sleep(1_000)
        }
    }
}

fun main() {
    Producer1().run()
}