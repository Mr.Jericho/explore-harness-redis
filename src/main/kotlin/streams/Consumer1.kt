package streams

import redis.clients.jedis.Jedis
import redis.clients.jedis.StreamEntryID
import redis.clients.jedis.StreamEntryID.LAST_ENTRY


/**
 * Following the presentation at https://www.youtube.com/watch?v=qXEyuUxQXZM
 * Demonstrates blocking XREAD  - see Consumer1Test
 */


class Consumer1(private val streamName: String = "stream", val callBack : (XREADResult) -> Unit) : Runnable {

    @Volatile
    var stop = false
    var lastId: StreamEntryID = LAST_ENTRY

    private val redis = Jedis("localhost")

    override fun run() {
        while (!stop) {
            redis.xread(0, 2 * 1_000, queryStream(streamName, lastId))?.let {
                lastId = it[0].value[0].id
                callBack(it)
            }
        }
    }
}
